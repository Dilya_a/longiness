$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')


// INCLUDE FUNCTION
 var styler 	= $(".styler:not(select)"),
	 owl		= $(".owl-carousel"),
	 select2    = $("select.styler"),
	 // scrollpane = $(".scroll-pane"),
	 popup      = $("[data-popup]"),
	 table      = $("#table_id");

	if(styler.length){
	  include("js/formstyler.js");
	}
	if(popup.length){
		include('js/jquery.arcticmodal.js');
	}

	if(select2.length){
	  include("js/select2.min.js");
	  include("js/jquery.nicescroll.min.js");
	  // include("js/jquery.mousewheel.shim.js");
	  // include("js/mwheelIntent.js");
	}

	if(owl.length){
	  include("js/owl.carousel.js");
	}

	// if($(table).length){
	  include("js/datatables.min.js");
	  include("js/dataTables.jqueryui.min.js");
	// }

	include("js/jquery.maskedinput.min.js")

	

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

		$("#nav-icon, .close_resp_menu").on('click ontouchstart', function(e){
	  		var body = $('body');
	  		
	  		body.toggleClass('nav_overlay');

	    })

	// SHOW/HIDDEN TABLE

			$("#author_btn").on("click", function(){
				$("body").addClass("rate_a");
			})
			$("#author_btn_close").on("click", function(){
				$("body").removeClass("rate_a");
			})


  	/* ------------------------------------------------
	MASKEDINPUT START
	------------------------------------------------ */

		$(function($){
			$(".date").mask("99");
			$(".year").mask("9999");
		});

	/* ------------------------------------------------
	MASKEDINPUT END
	------------------------------------------------ */


	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

		if (styler.length){

			styler.styler();
			// $("select").styler({
			// 	singleSelectzIndex: '999',
			//     onSelectOpened: function() {
			//       	scrollpane.jScrollPane();
			//     }
			// })
		}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */


	/* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

		if(owl.length){
			var sync1 = $("#sync1");
  			var sync2 = $("#sync2");
  			var sync3 = $("#sync3");

			sync1.owlCarousel({
				items : 1,
			    singleItem : true,
			    slideSpeed : 1000,
			    navigation: false,
			    pagination:false,
			    afterAction : syncPosition,
			    responsiveRefreshRate : 200,
			});
			 
			sync2.owlCarousel({
			items : 5,
			pagination:false,
			responsiveRefreshRate : 100,
			afterInit : function(el){
			  el.find(".owl-item").eq(0).addClass("synced");
			}
			});

			sync3.owlCarousel({
			items : 3,
			pagination:false,
			responsiveRefreshRate : 100,
			afterInit : function(el){
			  el.find(".owl-item").eq(0).addClass("synced");
			}
			});
		}

			function syncPosition(el){
				var current = this.currentItem;
				$("#sync2")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync2").data("owlCarousel") !== undefined){
				  center(current)
				}
			}
			function syncPosition2(el){
				var current = this.currentItem;
				$("#sync3")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync3").data("owlCarousel") !== undefined){
				  center(current)
				}
			}
			 
			$("#sync2").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			});

			$("#sync3").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			});
			 
			function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for(var i in sync2visible){
				  if(num === sync2visible[i]){
				    var found = true;
				  }
				}

				if(found===false){
				  if(num>sync2visible[sync2visible.length-1]){
				    sync2.trigger("owl.goTo", num - sync2visible.length+2)
				  }else{
				    if(num - 1 === -1){
				      num = 0;
				    }
				    sync2.trigger("owl.goTo", num);
				  }
				} else if(num === sync2visible[sync2visible.length-1]){
				  sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
				  sync2.trigger("owl.goTo", num-1)
				}

			}

			function center(number){
				var sync3visible = sync3.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for(var i in sync3visible){
				  if(num === sync3visible[i]){
				    var found = true;
				  }
				}

				if(found===false){
				  if(num>sync3visible[sync3visible.length-1]){
				    sync3.trigger("owl.goTo", num - sync3visible.length+2)
				  }else{
				    if(num - 1 === -1){
				      num = 0;
				    }
				    sync3.trigger("owl.goTo", num);
				  }
				} else if(num === sync3visible[sync3visible.length-1]){
				  sync3.trigger("owl.goTo", sync3visible[1])
				} else if(num === sync3visible[0]){
				  sync3.trigger("owl.goTo", num-1)
				}

			}



	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */

	/* ------------------------------------------------
	DATATABLE START
	------------------------------------------------ */

		$('.example').DataTable({
			"pageLength": 18
		});

	/* ------------------------------------------------
	DATATABLE END
	------------------------------------------------ */


	/* ------------------------------------------------
	POPUP START
	------------------------------------------------ */

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		beforeOpen: function(){
			    	}
			    });
			});
		};

	/* ------------------------------------------------
	POPUP END
	------------------------------------------------ */


	/* ------------------------------------------------
	SCROLLPANE START
	------------------------------------------------ */

		if (select2.length){
			// select2.select2();

			$("select.styler").each(function(){
		        var ret     = $(this).select2();
		        // custom scrollbars
		        var s;
		        ret.on("select2-open", function () {
		            if (!s) {
		                s = $('.select2-drop-active > .select2-results');
		                s.niceScroll({
		                    autohidemode: false,
		                    cursorcolor: "#002d51",
		                    cursorwidth: 8
		                });
		            }
		        });
		    });

		}

	/* ------------------------------------------------
	SCROLLPANE END
	------------------------------------------------ */
	

});